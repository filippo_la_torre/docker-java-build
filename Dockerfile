FROM openjdk:8

RUN echo 'deb https://apt.dockerproject.org/repo debian-jessie main' > /etc/apt/sources.list.d/docker.list; \
    apt-get update; \
    apt-get install docker-engine -y

RUN mkdir -p /root/.docker
